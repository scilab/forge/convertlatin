// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//

function subdemolist = demo_gateway()
  demopath = get_absolute_file_path("convertlatin.dem.gateway.sce");

  subdemolist = ["demo utf8tolatin","utf8tolatin.dem.sce"; ..
                 "demo latintoutf8","latintoutf8.dem.sce"];

  subdemolist(:,2) = demopath + subdemolist(:,2);
  
endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
