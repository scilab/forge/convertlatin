/* ==================================================================== */
/* Allan CORNET */
/* DIGITEO 2011 */
/* ==================================================================== */
#include <string.h>
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "utftolatin.h"
#ifdef _MSC_VER
#include "strdup_windows.h"
#endif
#include "localization.h"
#include "freeArrayOfString.h"
/* ==================================================================== */
int sci_utftolatin(char *fname)
{
    SciErr sciErr;
    int *piAddressVarOne = NULL;
    char **pStVarOne			= NULL;
    int *lenStVarOne			= NULL;
    int mOne = 0, nOne = 0;
    int mnOne = 0;

    char **pStVarOut			= NULL;

    int i = 0;

    Rhs = Max(0, Rhs);

    CheckRhs(1, 1);
    CheckLhs(1, 1);

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (!isStringType(pvApiCtx, piAddressVarOne))
    {
        Scierror(999,_("%s: Wrong type for input argument #%d: String expected.\n"), fname, 1);
        return 0;
    }
    sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne,&mOne, &nOne, NULL, NULL);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    mnOne = mOne * nOne;

    lenStVarOne = (int*)MALLOC(sizeof(int) * mnOne);
    if (lenStVarOne == NULL)
    {
        Scierror(999, _("%s: No more memory.\n"), fname);
        return 0;
    }

    sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne,&mOne, &nOne, lenStVarOne, NULL);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    pStVarOne = (char**) MALLOC(sizeof(char*) * mnOne);
    if (pStVarOne == NULL)
    {
        FREE(lenStVarOne); lenStVarOne = NULL;
        Scierror(999, _("%s: No more memory.\n"), fname);
        return 0;
    }

    for (i = 0; i < mnOne; i++)
    {
        pStVarOne[i] = (char*)MALLOC(sizeof(char) * (lenStVarOne[i] + 1));
        if (pStVarOne[i] == NULL)
        {
            freeArrayOfString(pStVarOne, i);
            if (lenStVarOne) {FREE(lenStVarOne); lenStVarOne = NULL;}
            Scierror(999, _("%s: No more memory.\n"), fname);
            return 0;
        }
    }

    sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne, &mOne, &nOne, lenStVarOne, pStVarOne);
    if (lenStVarOne) {FREE(lenStVarOne); lenStVarOne = NULL;}
    if(sciErr.iErr)
    {
        freeArrayOfString(pStVarOne, mnOne);
        printError(&sciErr, 0);
        return 0;
    }

    pStVarOut = (char**) MALLOC(sizeof(char*) * mnOne);
    if (pStVarOut == NULL)
    {
        freeArrayOfString(pStVarOne, mnOne);
        Scierror(999, _("%s: No more memory.\n"), fname);
        return 0;
    }


    for (i = 0; i < mnOne; i++)
    {
        pStVarOut[i] = utftolatin(pStVarOne[i]);
        if (pStVarOut[i] == NULL)
        {
            freeArrayOfString(pStVarOne, mnOne);
            freeArrayOfString(pStVarOut, mnOne);
            Scierror(999, _("%s: conversion error.\n"), fname);
            return 0;
        }
    }

    freeArrayOfString(pStVarOne, mnOne);

    sciErr = createMatrixOfString(pvApiCtx, Rhs + 1, mOne, nOne, pStVarOut);
    freeArrayOfString(pStVarOut, mnOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }
    LhsVar(1) = Rhs + 1;
    PutLhsVar();
    return 0;
}
/* ==================================================================== */

